FROM registry.gitlab.com/glauschwuffel/perldevenv

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get clean
RUN apt-get update && apt-get install -y \
    apt-file \
    build-essential \
    cpanminus \
    curl \
    dh-make-perl \
    libauthen-passphrase-perl \
    libdevel-cover-perl \
    libdist-zilla-app-command-cover-perl \
    libdist-zilla-perl \
    libdist-zilla-plugin-git-perl \
    libdist-zilla-plugin-installguide-perl \
    libdist-zilla-plugin-podweaver-perl \
    libdist-zilla-plugin-test-notabs-perl \
    libdist-zilla-plugin-test-perl-critic-perl \
    libmodule-cpanfile-perl \
    libperl-minimumversion-perl \
    libpod-coverage-trustpod-perl \
    libpod-markdown-perl \
    libppi-perl \
    libppix-regexp-perl \
    libtest-bdd-cucumber-perl \
    libtest-exception-perl \
    libtest-longstring-perl \
    libtest-net-ldap-perl \
    libtest-notabs-perl \
    libtest-pod-coverage-perl \
    libtest-pod-perl \
    libtest-spec-perl \
    libtest-warn-perl \
    libtest-warnings-perl \
    libtype-tiny-perl \
    libxml-libxml-perl \
    net-tools \
    unzip 

RUN apt-file update

# Set entrypoint so Gitlab is happy.
# See https://gitlab.com/gitlab-org/gitlab-runner/issues/1170 why this is needed
ENTRYPOINT [""]
